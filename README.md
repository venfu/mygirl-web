## himygirl应用端

## Build Setup

``` bash

# install cnpm
npm install -g cnpm

# install vue-cli
cnpm install -g vue-cli

# install quasar-cli
cnpm install -g quasar-cli@0.6.5

# install dependencies
$ cnpm install

# serve with hot reload at localhost:8080
$ quasar dev

# build for production with minification
$ quasar build

# lint code
$ quasar lint
```


#quasar.esm.js根目录下的这个文件替换quasar-framework@xx下的node-modules下的quasar的相同文件
解决首页menus中tabs点击导航，不折叠菜单的问题

# common-compo 组件：下拉刷新/回到顶部/内容回复
    -- topic-detail 
    -- reply-detail
    -- topic-list-compo

# 打包apk的流程
cnpm install -g cordova@4.1.1 //全局安装cordova,npm unistall cordova -g卸载
cordova platform add android //增加android 平台，在项目根目录生成cordova文件夹
你也添加其他：
cordova platform add wp8       
cordova platform add windows 
cordova platform add amazon-fireos 

cordova platform rm ios
cordova platform rm firefoxos
cordova platform rm windows

cordova platforms ls //查看
quasar build 打包资源->./dist目录下
将dist文件所有内容，copy到./cordova下，并命名为www文件夹

cd cordova 目录下
cordova build android //打包




## 加密解密测试

``` python

  let encryptor = new JSEncrypt()
        encryptor.setPublicKey('MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCdLX+DOAuH1f8KoXONkNv6/6tF6mk7r1mlsesNi/RKJ5yppTJXN9Oz1nO1Q5pOgzizIYJdz6ldxnZdq4iVPfjgI5WQacSM0/KiusaONM+JpIpRvVsaE0wZGy2AKxTcJTLD8Wa2sUD1fYCu/dTciQw8JdEkEeeXJwkURvCYZX/mQQIDAQAB')
        let rsaPassWord = encryptor.encrypt('This is a secret message')
        console.log('+密后的密码:',rsaPassWord);


// Decrypt with the private key...
          var decrypt = new JSEncrypt();
          decrypt.setPrivateKey('MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAJ0tf4M4C4fV/wqhc42Q2/r/q0XqaTuvWaWx6w2L9EonnKmlMlc307PWc7VDmk6DOLMhgl3PqV3Gdl2riJU9+OAjlZBpxIzT8qK6xo40z4mkilG9WxoTTBkbLYArFNwlMsPxZraxQPV9gK791NyJDDwl0SQR55cnCRRG8Jhlf+ZBAgMBAAECgYACdosQAFnV6BmWDg+kgUehOE5zyweZItdRzGb1kGLkophG5ErUqRBNOigYCBYr8sx5qiXD2JQ73vdN4nlCdXUn77z1qXzSesEEnx5hyraZWUxKCTde5Wfh7kQAJx2eb2ccg4+qKt5I1fPTxeAhJYiTq+JE0/qSjSpnImsoSCojQQJBAM0xHf6U+frgM1DATFmw12XN7znBR7qwv4DDHl7xlInEThtWUqc6h59n5uc8oEBvHycJTHhJP2roDUkN7oS3HIkCQQDEGNGHO4v5WmFrxLi73R9zbAlmFe4SHmHKyn7u19zqEIGiK+DKWUKvUW8sTjJlmRLo3BrXVmu94qKUnu/XtL35AkA2IKbQ6WuoCkx/GLsLTeMEL/bTPXE2hjl+NfplAUU1slsISxUyl/X+zuHSTePYG3gUTBeoA2p/QQeAJfStfcBBAkAww18vOr6HBlOdgfDSPp14jVAVA7UM6mM1E3/XEvTLe8lQs4qVpvavNBfqM5PSsesC3uEJ1WK7eGUnghJwWHC5AkB2nJ/iYeWemyofltMb/jqaVaLwA5A6v5a4Vb6d0QWeUBWDAqL2Q6T1oCSTWFJHfOSDOXK4EJw5ch7a7n+ia5zS');
          var uncrypted = decrypt.decrypt(rsaPassWord);
console.log('解密后的密码:',uncrypted);

```

## PouchDb设置cookie

``` python


/**
 * PouchDb-Store
 */
import PouchDB from "pouchdb";
import { log } from "core-js";
var tokenDb = new PouchDB("tokens");

export default {
  tokenId() {
    return "usertoken";
  },
 /**
  * 存储token到pouchDb中(如果存在,覆盖)
  * @param {* token的健} tokenK
  * @param {* token的值} tokenV 
  */
  tokenDbAdd(tokenK,tokenV) {
      console.log('存储到tokenDb:k=',tokenK,',tokenV=',tokenV);
      if(!tokenV) return false;
      tokenDb.put({
        _id: tokenK,
        token: tokenV
      })
      .then(res => {
        if (res.ok) {
          console.log('存储token成功')
        }
      })
      .catch(err => {
        console.log('存储token异常:',err)
      })
  },
  tokenDbGetAndSetCookie(tokenK) {
    if(!document.cookie.indexOf('remember-me')) {
      console.log('remember-me的cookie已经存在')
      return false;
    }
    tokenDb
    .get(tokenK)
    .then(_token => {
      console.log('获取token结果:',_token)
      if(_token.token) {
        //手动设置remember-me到cookie,删除setCookie(name, "", -1);  
        // document.cookie="remember-me2="+_token.token+";Path=/;Max-Age=-1;";
        // document.cookie="remember-me="+_token.token+";Path=/;Max-Age=2592002;";
      }
    })
    .catch(err => {
      console.log('获取token失败:',err)
    })
  }
}

 if(_token.token) {
    //手动设置remember-me到cookie,删除setCookie(name, "", -1);  
    // document.cookie="remember-me2="+_token.token+";Path=/;Max-Age=-1;";
    // document.cookie="remember-me="+_token.token+";Path=/;Max-Age=2592002;";
 }

```