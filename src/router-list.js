export default [
  {
    title: 'girl区',
    icon: 'people',
    hash: 'community',
    features: [
      {
        title: '我的girl区',
        icon: 'people',
        hash: 'my-topic',
        ismenu: true
      },
      {
        title: '主题详情',
        icon: 'bubble chart',
        hash: 'topic-detail'
      },
      {
        title: '回复详情',
        icon: 'comment',
        hash: 'reply-detail'
      }
    ]
  },
  {
    title: 'girl城',
    icon: 'shop two',
    hash: 'mall',
    features: [
      {
        title: 'higirl商城(beta)',
        icon: 'assignment',
        hash: 'gooitem',
        ismenu: true
      },
      {
        title: '购物券',
        icon: 'store',
        hash: 'coupon-item',
        ismenu: true
      },
      {
        title: '商品详情',
        icon: 'details',
        hash: 'goo-detail'
      },
      {
        title: '购物车',
        icon: 'shopping cart',
        hash: 'goo-car'
      }
    ]
  },
  {
    title: '个人中心',
    icon: 'person',
    hash: 'personal',
    features: [
      {
        title: '我的通知',
        icon: 'email',
        hash: 'my-partake',
        requireAuth: true
      },
      {
        title: '我的订阅',
        icon: 'rss feed',
        hash: 'my-subscribe',
        requireAuth: true
      },
      {
        title: '关注信息',
        icon: 'rss feed',
        hash: 'attentions-info'
      },
      {
        title: '设置',
        icon: 'settings',
        hash: 'sys-settings',
        requireAuth: true
      },
      {
        title: '个人主页',
        icon: 'person',
        hash: 'personal-home',
        ismenu: true,
        requireAuth: true
      }
    ]
  },
  {
    title: '系统',
    icon: 'stay current portrait',
    hash: 'sys',
    features: [
      {
        title: '系统消息',
        icon: 'record voice over',
        hash: 'sys-notify'
      },
      {
        title: '关于',
        icon: 'info',
        hash: 'aboutme'
      },
      {
        title: '测试',
        icon: 'record voice over',
        hash: 'demo'
      },
      {
        title: '图片压缩',
        icon: 'view day',
        hash: 'sys-pic-compress'
      },
      {
        title: '实验室',
        icon: 'move to inbox',
        hash: 'sys-experience'
      }
    ]
  }

]
