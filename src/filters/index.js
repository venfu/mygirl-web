import {
  date
} from "quasar"
const {
  formatDate
} = date

/**
 * 格式化日期成时间：
 * @param {*日期} _date  
 */
export function formatDateToTime(_date) {
  if (_date) {
    return formatDate(_date, 'YYYY-MM-DD HH:mm')
  }
  return _date;
}
/**
 * 格式化数值，k,w 
 * 如：1200 -> 1.2k
 * @param {* 数值}  num
 */
export function convertNum(num) {
  if (!num) return 0;
  const _num = Number.parseFloat(num)
  if (_num < 10000) {
    return _num;
  }
  // else if (_num >= 1000 && _num < 10000) {
  //   return (_num / 1000).toFixed(1) + 'k';
  // } 
  else if (_num >= 10000) {
    return (_num / 10000).toFixed(1) + 'w';
  }
}

/**
 * 关注的操作名称
 * @param {*是否是共同类型} isTogether 
 * @param {*类型：粉丝or关注} type 
 * @param {*是否是个人,他人} isself 
 * @param {*是否是相互的} isMutual 
 */
export function attentOperateName(isTogether, type, isself, isMutual) {
  // console.log('处理关注操作的名称：');
  // console.log('isTogether=',isTogether);
  // console.log('type=',type);
  // console.log('isself=',isself);
  // console.log('isMutual=',isMutual);

  if (isself) {
    if (!type || type == 'fans') {
      return isMutual ? '相互关注' : '+ 关注'
    } else if (type == 'attentions') {
      return isMutual ? '⇄ 取消' : '→ 取消'
    }
  } else {
    if (!type || type == 'fans') {
      return isTogether ? '共同粉丝' : '+ 关注'
    } else if (type == 'attentions') {
      return isTogether ? '共同关注' : '+ 关注'
    }
  }
}

/**
 * 内容长度截取替换...
 * @param {*内容} content 
 */
export function contentLengthEllipsis(content, _length) {
  if (!content) return content;
  if (!_length || !Number.parseInt(_length)) {
    _length = 50
  }
  if (content.length > _length) {
    return content.substring(0, _length) + '......';
  } else {
    return content;
  }
}
/**
 * 内容长度换行
 * @param {*内容} content 
 */
const splitMark = '<br/>'
export function contentLengthWrap(content) {
  if (!content) return content;
  let newArr = [];
  newArr.push(content)
  // const arrs = content.split(/(.{50})/).filter(a=>a);
  // if (arrs) {
  //   newArr.push(arrs.join(splitMark));
  // } else {
  //   newArr.push(content);
  // }
  return newArr;
}
/**
 * 字符串转数组
 * @param {字符串值} _str 
 */
export function strToArr(_str) {
  if (_str) {
    let arr = [];
    arr.push(_str);
    return arr;
  }
}

/**
 * 处理图片资源的回显
 * @param {*图片资源} paths
 */
export function attashView(paths) {
  if (paths) {
    return paths.map(a => {
      return global.API_PREFIX + a;
    })
  }
}
/**
 * 头像资源的回显
 * @param {*图片资源} path
 */
export function avatarView(path) {
  if (path && !path.startsWith('statics/')) {
    return global.API_PREFIX + path;
  } else {
    return path;
  }
}
/**
 * 手机号覆盖星号
 * @param {*手机号} mo 
 */
export function mobileCoverFormat(mo) {
  if (mo) {
    return mo.replace(/(\d{3})\d{4}(\d{3})/, '$1****$2');
  }
  return mo;
}

/**
 * 邮箱覆盖星号
 * @param {*邮箱地址} em 
 */
export function emailCoverFormat(em) {
  if (em) {
    return em.replace(/(.{2}).+(.{2}@+)/, '$1****$2');
  }
  return em;
}

/**
 * 字符串剔除转义&剔除首尾"
 * @param {*} str 
 */
export function escapeAndRmQuote(_content) {
  if (!_content) {
    return;
  }
  let str = decodeURIComponent(_content);
  str = str.replace(/\\/, '');
  if (str.startsWith('"')) {
    str = str.substring(1, str.length);
  }
  if (str.endsWith('"')) {
    str = str.substring(0, str.length - 1);
  }
  return str;
}
