/**
 * 图片多选工具(cordova-plugin-imagepicker-plus和cordova-android-support-gradle-release插件的共同使用)
 * 注意在index.html中指定一个列表回显标签：<div id="img_list"></div>
 */
export default {
  showImage(list) {
    var imgContainer = document.getElementById("img_list");
    imgContainer.innerHTML = "";
    for (var x in list) {
      if (list.hasOwnProperty(x)) {
        (function (x) {
          var div = document.createElement("div");
          // 创建对象
          var img = new Image();
          img.onload = function () {
            var node = document.createElement('p');
            node.innerHTML = "onload naturalWidth:" + img.naturalWidth + ",naturalHeight:" + img.naturalHeight;
            div.appendChild(node);
          };
          img.src = list[x];
          img.width = 20;
          var p = document.createElement("p");
          p.innerHTML = "naturalWidth:" + img.naturalWidth + ",naturalHeight:" + img.naturalHeight;
          div.appendChild(p);
          div.appendChild(img);
          imgContainer.appendChild(div);
        })(x)
      }
    }
  }
}

