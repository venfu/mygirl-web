
/**
 * PouchDb-Store
 */
import PouchDB from "pouchdb";
import { log } from "core-js";
var tokenDb = new PouchDB("tokens");
var versionDb = new PouchDB("appversion");

export default {
  tokenId() {
    return "usertoken";
  },
  versionId() {
    return "appversion";
  },
 /**
  * 存储token到pouchDb中(如果存在,覆盖)
  * @param {* token的健} tokenK
  * @param {* token的值} tokenV 
  */
  tokenDbAdd(tokenK,account,password) {
      console.log('存储到tokenDb:k=',tokenK,',account=',account,',password=',password);
      if(!account) return false;
      tokenDb.put({
        _id: tokenK,
        account: account,
        password: password
      })
      .then(res => {
        if (res.ok) {
          console.log('存储token成功')
        }
      })
      .catch(err => {
        console.log('存储token异常:',err)
      })
  },
  tokenDbUpdate(tokenK,account) {
    console.log('更新tokenDb:k=',tokenK,',account=',account);
    if(!verV) return false;
    versionDb.get(tokenK).then(function(_token) {
      return db.put({
        _id: tokenK,
        account: account,
        password: _token.password
      })
    }).then(function(response) {
    }).catch(function (err) {
      console.log(err);
    })
   },
  tokenDbGet(tokenK,callback){
    tokenDb
    .get(tokenK)
    .then(_token => {
      console.log('获取token结果:',_token)
      callback(_token);
    })
    .catch(err => {
      console.log('获取token失败:',err)
    })
  },
  tokenDbRemove(tokenK) {
    tokenDb.get(tokenK).then(function(doc) {
      return tokenDb.remove(doc);
    }).then(function (res) {
      console.log('移除token成功:',res)
    }).catch(function (err) {
      console.log('移除token失败:',err)
    });
  },

  /******versionDb*******/

  versionDbGet(verK,callback){
    versionDb
    .get(verK)
    .then(_version => {
      console.log('获取appversion结果:',_version)
      callback(_version);
    })
    .catch(err => {
      console.log('获取appversion失败:',err)
    })
  },
  versionDbAdd(verK,verV) {
    console.log('新增: versionDb:k=',verK,',v=',verV);
    if(!verV) return false;
    versionDb.put({
      _id: verK,
      version: verV
    })
    .then(res => {
      if (res.ok) {
        console.log('存储appversion成功')
      }
    })
    .catch(err => {
      console.log('存储appversion异常:',err)
    })
   },
   versionDbUpdate(verK,verV) {
    console.log('更新: versionDb:k=',verK,',v=',verV);
    if(!verV) return false;
    versionDb.get(verK).then(function(doc) {
      return db.put({
        _id: verK,
        _rev: doc._rev,
        title: verV
      })
    }).then(function(response) {
    }).catch(function (err) {
      console.log(err);
    })
   }
}