import { Alert } from 'quasar'
import '../statics/js/stomp'
import '../statics/js/sockjs.min.js'
/**
 * websocket连接
 */
var stompClient = null;
export default {
  connect() {
    var socket = new SockJS('/wsPoint');//连接服务端的端点，连接以后才可以订阅广播消息和个人消息 
    
    stompClient =  Stomp.over(socket); 
    console.log('连接:',stompClient);
    stompClient.connect({}, function(frame) {
      console.log('Connected:' + frame);
      //订阅广播消息
      stompClient.subscribe('/topic/broadcast', function (response) {
        console.log('收到广播消息：',response)
        alertMsg(response.body);
      });
      //订阅个人信息
      stompClient.subscribe('/user/topic', function (response) {
        console.log('收到订阅消息：',response)
        alertMsg(response.body);
      })
    },function onFailed(){});
  },
  disconnect() {
    if (stompClient != null) {
      stompClient.disconnect();
    }
    console.log('Disconnected');
  },
  alertMsg(msg) {
    if(msg){
      const _errAlert = Alert.create({
        color: 'warning',
        icon: 'info',
        html: msg,
        position: 'bottom',
      });
      setTimeout(()=>{
        _errAlert.dismiss();
      },3000);
    }
  }
}

