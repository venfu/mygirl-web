
/**
 * sessionStorage
 */

export default {
  USER_KEY:"USERINFO",
  AD_RECORD_KEY:"AD_RECORD_KEY",
  SYSINFO:"SYSINFO",
  saveuser(user){
    window.sessionStorage.setItem('USERINFO', JSON.stringify(user))
  },
  curuser() {
    return JSON.parse(window.sessionStorage.getItem('USERINFO'))
  },
  curuserId() {
    const _user = JSON.parse(window.sessionStorage.getItem('USERINFO'));
    if(_user) {
      return _user.id;
    } else {
      return null;
    }
  },
  removeCuruser() {
    this.remove('USERINFO')
  },

  saveByLong (key, val) {
    window.localStorage.setItem(key, val)
  },
  fetchFromLong (key) {
    return window.localStorage.getItem(key)
  },
  clearByLong (key) {
    window.localStorage.removeItem(key)
  },
  save (key, val) {
    window.sessionStorage.setItem(key, JSON.stringify(val))
  },
  fetch (key) {
    return JSON.parse(window.sessionStorage.getItem(key))
  },
  clear () {
    window.sessionStorage.clear()
  },
  remove (key) {
    window.sessionStorage.removeItem(key)
  }
}

