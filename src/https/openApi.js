import { openApi } from './apiUrl'
import axios from './httpUtils'

export default {
  /**
   * 获取当前版本信息
   */
  getAppVersion() {
    return axios({
      method: 'get',
      url: openApi.getAppVersion()
    })
  },
  /**
   * 获取当前AppKey信息
   */
  getAppKeyDesc() {
    return axios({
      method: 'get',
      url: openApi.getAppKeyDesc()
    })
  },
  /**
   * 添加appKey的接口次数
   * 参数格式：{customerId:?,apiName:?,times:?}
   * @param {*} params 
   */
  addAppKeyTimes(params) {
    return axios({
      method: 'put',
      url: openApi.addAppKeyTimes(),
      data: params
    })
  },
}
