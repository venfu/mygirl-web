import { communityCollectApi } from './apiUrl'
import axios from './httpUtils'

export default {
  
  /**
   * 添加收藏
   * 参数格式：{topicId:?}
   * @param {*} params 
   */
  add(params) {
    return axios({
      method: 'post',
      url: communityCollectApi.addUrl(),
      data: params
    })
  },
  /**
   * 取消收藏
   * 参数格式：{topicId:?}
   * @param {*} params 
   */
  cancel(params) {
    return axios({
      method: 'put',
      url: communityCollectApi.cancelUrl(),
      data: params
    })
  },
}
