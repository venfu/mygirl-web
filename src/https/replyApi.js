import { communityReplyApi } from './apiUrl'
import axios from './httpUtils'

export default {
  /**
   * 列表查询
   * 参数格式： ?size=20&replyTarid=?&isTopic=?
   * @param {*} params 
   */
  list(params) {
    return axios({
      method: 'get',
      url: communityReplyApi.listUrl(params)
    })
  },
  /**
   * boolean isTopic;// 是否是回复的主题
   * Long replyTarid;// 回复目标内容ID（回复主题or回复回复）
   * Customer replyTaruser;
   * String content;// 回复内容
   * @param {*} params 
   */
  add(params) {
    return axios({
      method: 'post',
      url: communityReplyApi.addUrl(),
      data: params
    })
  },
  delete(id) {
    return axios({
      method: 'delete',
      url: communityReplyApi.deleteUrl(id)
    })
  },
  get(id) {
    return axios({
      method: 'get',
      url: communityReplyApi.getUrl(id)
    })
  },
  liked(id) {
    return axios({
      method: 'put',
      url: communityReplyApi.likedUrl(id)
    })
  },
  likedCancel(id) {
    return axios({
      method: 'put',
      url: communityReplyApi.likedCancelUrl(id)
    })
  },
}
