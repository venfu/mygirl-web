import { notifyApi } from './apiUrl'
import axios from './httpUtils'

export default {
  /**
 * 系统提醒的标识
 *返回参数格式：{SYS_NOTIFY:false,USER_NOTIFY_COUNT:5}
 */
  notifyMark() {
    return axios({
      method: 'get',
      url: notifyApi.notifyMarkUrl(),
    })
  },
  /**
   * 消息提醒列表
   * 参数格式：?isSys=false
   */
  list(params) {
    return axios({
      method: 'get',
      url: notifyApi.listUrl(params),
    })
  },
  /**
   * 全部已读
   */
  readAll() {
    return axios({
      method: 'put',
      url: notifyApi.readAllUrl()
    })
  },
  /**
   * 单个已读
   */
  readOne(id) {
    return axios({
      method: 'put',
      url: notifyApi.readOneUrl(id)
    })
  },

}
