import { commonApi } from './apiUrl'
import axios from './httpUtils'

export default {
  retrieveFile(fileName) {
    return axios({
      method: 'get',
      url: commonApi.retrieveFileUrl(fileName)
    })
  },
  uploadFiles(files) {
    return axios({
      method: 'post',
      url: commonApi.uploadFilesUrl(),
      data: files,
      headers: { 'Content-Disposition': 'multipart/form-data'}
    })
  },
  uploadFile(file) {
    return axios({
      method: 'post',
      url: commonApi.uploadFileUrl(),
      data: file,
      headers: { 'Content-Disposition': 'multipart/form-data'}
    })
  },
  uploadFilesCompress(files,width,height) {
    return axios({
      method: 'post',
      url: commonApi.uploadFilesUrlCompress(width,height),
      data: files
    })
  }
}
