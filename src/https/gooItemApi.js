import { gooItemApi } from './apiUrl'
import axios from 'axios'

export default {
    getOne(gooId) {
        return axios({
            method: 'get',
            url: gooItemApi.getOneUrl(gooId)
        })
    },
    getList(params) {
        return axios({
            method: 'get',
            url: gooItemApi.getListUrl(params)
        })
    }
}