import { communityReportApi } from './apiUrl'
import axios from './httpUtils'

export default {
  /**
   * 添加举报
   * {reportType:USER/TOPIC/REPLY,targetuser:'',message:'',targetContent:'',topicId:'',replyId:''}
   * @param {*} params 
   */
  add(params) {
    return axios({
      method: 'post',
      url: communityReportApi.addUrl(),
      data: params
    })
  },
  /**
   * 登录人的举报列表
   */
  list() {
    return axios({
      method: 'get',
      url: communityReportApi.listUrl(),
    })
  },

}
