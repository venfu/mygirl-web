import { customerApi } from './apiUrl'
import axios from 'axios'

export default {
  /**
   * 账户密码登录
   * @param {*} params 登录参数 account=?&password=?&isAutoLogin=true
   */
  loginAccount(params) {
    return axios({
      method: 'post',
      url: customerApi.loginAccountUrl(),
      data: params,
      headers: { 'X-Requested-With': 'XMLHttpRequest', 'Content-Type': 'application/x-www-form-urlencoded' }
    })
  },
  /**
   * 手机号验证码登录/注册
   * @param {*} params 登录参数  mobile=?&captcha=?&isAutoLogin=true
   */
  loginCaptcha(params) {
    return axios({
      method: 'post',
      url: customerApi.loginCaptchaUrl(),
      data: params,
      headers: { 'X-Requested-With': 'XMLHttpRequest', 'Content-Type': 'application/x-www-form-urlencoded' }
    })
  },
  /**
   * 登出账户
   */
  logout() {
    return axios({
      method: 'get',
      url: customerApi.logoutUrl()
    })
  },
  /**
   * 当前账户信息
   */
  userinfo() {
    return axios({
      method: 'get',
      url: customerApi.userInfoUrl(),
    })
  },
  /**
   * 个人主页
   */
  personalHome(id) {
    return axios({
      method: 'get',
      url: customerApi.personalHomeUrl(id),
    })
  },
  setNickname(nickname) {
    return axios({
      method: 'put',
      url: customerApi.setNicknameUrl(nickname),
    })
  },
  setSummary(summary) {
    return axios({
      method: 'put',
      url: customerApi.setSummaryUrl(summary),
    })
  },
  setSex(sex) {
    return axios({
      method: 'put',
      url: customerApi.setSexUrl(sex),
    })
  },

  setLogoRes(logoRes) {
    return axios({
      method: 'put',
      url: customerApi.setLogoresUrl(logoRes),
    })
  },
  setPwd(password) {
    return axios({
      method: 'put',
      url: customerApi.setPwdUrl(password),
    })
  },
  /**
   * 添加设备信息
   * 参数格式：{customerId:?,platform:?,version:?,versionNumber:?}
   * @param {*设备信息} customerDevice 
   */
  setCommonDevice(customerDevice) {
    return axios({
      method: 'put',
      url: customerApi.setCommonDeviceUrl(),
      data: customerDevice
    })
  },
  /**
   * 更改密码
   * 参数格式： {originPwd:?,newPwd:?}
   * @param {*新旧密码信息} pwdinfo 
   */
  setPwdCheck(pwdinfo) {
    return axios({
      method: 'put',
      url: customerApi.setPwdCheckUrl(),
      data: pwdinfo
    })
  },
  /**
   * 重置密码
   * @param {*} pwdinfo 
   */
  resetPwd(pwdinfo) {
    return axios({
      method: 'put',
      url: customerApi.resetPwdUrl(),
      data: pwdinfo
    })
  },
  /**
   * 发送邮箱验证
   * 参数格式： {email:?,msgType:BIND_VALIDATE}
   * @param {*} params 
   */
  sendValidateEmail(params,customerId) {
    return axios({
      method: 'put',
      url: customerApi.sendValidateEmailUrl(customerId),
      data: params
    })
  },
  /**
   * 修改绑定信息 
   * 参数格式：{bindinfoType:?,value:?,orgValue:?,captcha:?}
   * @param {*} bindinfo 
   */
  updateBindinfo(bindinfo) {
    return axios({
      method: 'put',
      url: customerApi.updateBindinfoUrl(),
      data: bindinfo,
    })
  },

  /**
   * 邮箱注册
   * 参数格式：{nickname:?,password:?,email:?,captcha:?}
   * @param {*} params 
   */
  registerByemail(params) {
    return axios({
      method: 'put',
      url: customerApi.regByemailUrl(),
      data: params
    })
  },
  /**
   * 手机号注册
   * 参数格式：{nickname:?,password:?,mobile:?,captcha:?}
   * @param {*} params 
   */
  registerBymobile(params) {
    return axios({
      method: 'put',
      url: customerApi.regBymobildUrl(),
      data: params
    })
  },

  /**
   * 发送邮箱验证码
   * 参数格式： {email:?,msgType:?}
   * @param {*} params 
   */
  sendCaptchaEmail(params) {
    return axios({
      method: 'put',
      url: customerApi.sendCaptchaEmailUrl(),
      data: params
    })
  },
  /**
   * 发送手机验证码
   * 参数格式： {mobile:?}
   * @param {*} params 
   */
  sendCaptchaMobile(params) {
    return axios({
      method: 'put',
      url: customerApi.sendCaptchaMobileUrl(),
      data: params
    })
  },
}
