import { communityAttentionApi } from './apiUrl'
import axios from './httpUtils'

export default {
  /**
   * (指定人)关注列表
   * @param {*指定人ID} fromId 
   */
  attentionsList(fromId) {
    return axios({
      method: 'get',
      url: communityAttentionApi.attentionsListUrl(fromId)
    })
  },
  /**
   * (指定人)粉丝列表
   * @param {*指定人ID} toId 
   */
  fansList(toId) {
    return axios({
      method: 'get',
      url: communityAttentionApi.fansListUrl(toId)
    })
  },
  /**
   * 添加关注
   * 参数格式：{toId:?}
   * @param {*} params 
   */
  add(params) {
    return axios({
      method: 'post',
      url: communityAttentionApi.addUrl(),
      data: params
    })
  },
  /**
   * 取消关注
   * 参数格式：{toId:?}
   * @param {*} params 
   */
  cancel(params) {
    return axios({
      method: 'put',
      url: communityAttentionApi.cancelUrl(),
      data: params
    })
  },
}
