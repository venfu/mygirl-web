import {
  couponItemApi
} from './apiUrl'
import axios from 'axios'

export default {
  takeOne (auctionid) {
    return axios({
      method: 'get',
      url: couponItemApi.takeOneUrl(auctionid)
    })
  },
  getList (params) {
    return axios({
      method: 'get',
      url: couponItemApi.getListUrl(params)
    })
  }
}
