/**
 * const baseUri = process.env.NODE_ENV === 'production' ? 'http://127.0.0.1:8099' : 'http://127.0.0.1:3000'
 * @vencano 如果开发未进行前后分离：可定义url 常量，然后，如：
 *  export const testApi = {
 *    testGetUrl: () => baseUri+'/getone',
 *    testPostUrl: id => baseUri+'/modified/'+id
 *  }
 */

var baseUri = process.env.NODE_ENV === 'production' ? (global.IS_ANDROID ? 'http://c.himygirl.cn/api' : '/api') : '/api'


/**
 * 通用开放接口
 */
const openBaseUri = baseUri + '/general/open'
export const openApi = {
  // GET
  getAppVersion: () => openBaseUri + '/appversion',
  // GET
  getAppKeyDesc: () => openBaseUri + '/appkey/desc',
  // POST
  addAppKeyTimes: () => openBaseUri + ' /appkey/addtime'
}

/**
 * 文件上传加载接口
 */
export const commonApi = {
  // POST
  uploadFilesUrl: () => baseUri + '/files/upload',
  // POST
  uploadFileUrl: () => baseUri + '/files/upload-one',
  //POST 压缩大小
  uploadFilesUrlCompress: (width, height) => baseUri + '/files/upload?width=' + width + '&height=' + height,
  // GET
  retrieveFileUrl: fileName => baseUri + '/files/retrieve/' + fileName
}

const notifyBaseUri = baseUri + '/dashboard'
export const notifyApi = {
  // GET
  notifyMarkUrl: () => notifyBaseUri,
  //GET 消息列表
  listUrl: (params) => notifyBaseUri + '/notifys' + params,
  // PUT
  readAllUrl: () => notifyBaseUri + '/usernotifys/readall',
  // PUT
  readOneUrl: (id) => notifyBaseUri + '/usernotifys/readone?id=' + id
}

/**
 * 社区
 */
const communityTopicBaseUri = baseUri + '/community/topic'
export const communityTopicApi = {
  //GET ?size=20&refType=?&topicCate=?&refId=?
  listUrl: (params) => communityTopicBaseUri + params,
  // POST
  addUrl: () => communityTopicBaseUri,
  // DELETE
  deleteUrl: (id) => communityTopicBaseUri + '/' + id,
  // PUT 
  likedUrl: (id) => communityTopicBaseUri + '/' + id + '/liked',
  // PUT 
  likedCancelUrl: (id) => communityTopicBaseUri + '/' + id + '/liked/cancel',
  // GET
  getUrl: (id) => communityTopicBaseUri + '/' + id,

}

const communityReplyBaseUri = baseUri + '/community/reply'
export const communityReplyApi = {
  //GET ?size=20&replyTarid=?&isTopic=?
  listUrl: (params) => communityReplyBaseUri + params,
  // POST 
  addUrl: () => communityReplyBaseUri,
  // DELETE
  deleteUrl: (id) => communityReplyBaseUri + '/' + id,
  // PUT 
  likedUrl: (id) => communityReplyBaseUri + '/' + id + '/liked',
  // PUT 
  likedCancelUrl: (id) => communityReplyBaseUri + '/' + id + '/liked/cancel',
  // GET
  getUrl: (id) => communityReplyBaseUri + '/' + id,

}


// 关注
const communityAttentionBaseUri = baseUri + '/community/attention'
export const communityAttentionApi = {
  // POST {fromId:?,toId:?}
  addUrl: () => communityAttentionBaseUri,
  // PUT
  cancelUrl: () => communityAttentionBaseUri + '/cancel',
  // GET
  attentionsListUrl: (fromId) => communityAttentionBaseUri + '/attentions?size=20&fromId=' + fromId,
  // GET
  fansListUrl: (toId) => communityAttentionBaseUri + '/fans?size=20&toId=' + toId,
}

// 收藏
const communityCollectBaseUri = baseUri + '/community/collect'
export const communityCollectApi = {
  // POST {fromId:?,toId:?}
  addUrl: () => communityCollectBaseUri,
  // PUT
  cancelUrl: () => communityCollectBaseUri + '/cancel'
}

const communityReportBaseUri = baseUri + '/community/report'
export const communityReportApi = {
  // POST {reportType:?,targetuser:?,topicId:?,replyId:?,targetContent:?,message:?}
  addUrl: () => communityReportBaseUri,
  // GET
  listUrl: () => communityReportBaseUri,
}

/**
 * 客户模块接口
 */
const customerBaseUri = baseUri + '/customers'
export const customerApi = {
  // POST  application/x-www-form-urlencoded account=?&password=?&isAutoLogin=true
  loginAccountUrl: () => baseUri + '/login/account',
  // POST  application/x-www-form-urlencoded mobile=?&captcha=?&isAutoLogin=true
  loginCaptchaUrl: () => baseUri + '/login/captcha',
  // GET
  logoutUrl: () => baseUri + '/logout',
  // GET 客户主页
  personalHomeUrl: (id) => customerBaseUri + '/personal-home?id=' + id,
  // PUT
  setNicknameUrl: (nickname) => customerBaseUri + '/set-nickname?nickname=' + nickname,
  setSexUrl: (sex) => customerBaseUri + '/set-sex?sex=' + sex,
  setSummaryUrl: (summary) => customerBaseUri + '/set-summary?summary=' + summary,
  setLogoresUrl: (logoRes) => customerBaseUri + '/set-logores?logoRes=' + logoRes,
  setPwdUrl: (password) => customerBaseUri + '/set-pwd?password=' + password,
  // PUT {customerId:?,platform:?,version:?,versionNumber:?}
  setCommonDeviceUrl: () => customerBaseUri + '/set-device',
  // PUT {id:?,originPwd:?,newPwd:?}
  setPwdCheckUrl: () => customerBaseUri + '/set-pwd-check',
  //PUT 修改绑定信息 {bindinfoType:?,value:?,orgvalue:?,captcha:?}
  updateBindinfoUrl: () => customerBaseUri + '/update-bindinfo',
  // PUT {nickname:?,password:?,email:?,captcha:?}
  regByemailUrl: () => customerBaseUri + '/register-byemail',
  // PUT {nickname:?,password:?,mobile:?,captcha:?}
  regBymobildUrl: () => customerBaseUri + '/register-bymobile',

  // GET
  userInfoUrl: () => customerBaseUri + '/open/info',
  // PUT 修改密码
  resetPwdUrl: () => customerBaseUri + '/open/reset-pwd',
  // PUT 发送邮箱验证码 {email:?,msgType:?}
  sendCaptchaEmailUrl: () => customerBaseUri + '/open/send-captcha/email',
  // PUT 发送手机号验证码 {mobile:?}
  sendCaptchaMobileUrl: () => customerBaseUri + '/open/send-captcha/mobile',
  // PUT 发送校验邮箱: {email:?,msgType:BIND_VALIDATE}
  sendValidateEmailUrl: (customerId) => customerBaseUri + '/open/send-validate/email?customerId=' + customerId


}

const customerAddressBaseUri = baseUri + '/customers/address';

/**
 * 客户地址
 * {customerId:?,contactUser:?,contactPhone:?,zipcode:?,brief:?,details:?,isDefault:?}
 */
export const customerAddressApi = {
  // GET 
  findByCustomerUrl: (customerId) => customerAddressBaseUri + '/by-customer?customerId=' + customerId,
  // POST
  addUrl: () => customerAddressBaseUri,
  // DELETE
  deleteUrl: (id) => customerAddressBaseUri + '/' + id,
  // PUT 
  updateUrl: (id) => customerAddressBaseUri + '/' + id,
  // GET
  getUrl: (id) => customerAddressBaseUri + '/' + id,
}


/**
 * 商品分类接口 /goo/item/cates
 */
const gooItemCateBaseUri = baseUri + '/goo/item/cates';
export const gooItemCateApi = {
  // GET
  getOneUrl: cateId => gooItemCateBaseUri + '/' + cateId,
  // GET
  getListUrl: () => gooItemCateBaseUri,
  // GET
  searchUrl: (keywords) => !keywords ? gooItemCateBaseUri + '/search' : gooItemCateBaseUri + '/search?keywords=' + keywords
}
/**
 * 商品
 */
const gooItemBaseUri = baseUri + '/goo/items'
export const gooItemApi = {
  // GET
  getUrl: gooId => gooItemBaseUri + '/' + gooId,
  // GET
  getListUrl: params => !params ? gooItemBaseUri : gooItemBaseUri + params,
  // PUT {itemDesc:?,attributes:[{name:?,opitons:[]}]}
  updateDescUrl: gooId => gooItemBaseUri + '/' + gooId + '/desc'
}

/**
 * 购物券
 */
const couponItemUri = baseUri + '/coupon/tklm'
export const couponItemApi = {
  // GET
  getListUrl: params => !params ? couponItemUri + '/search' : couponItemUri + '/search' + params,
  takeOneUrl: auctionid => couponItemUri + '/take?auctionid=' + auctionid
}
