import { communityTopicApi } from './apiUrl'
import axios from './httpUtils'

export default {
  /**
   * 列表查询
   * 参数格式：?size=20&refType=?&topicCate=?&refId=?&lzId=?
   * @param {*} params 
   */
  list(params) {
    return axios({
      method: 'get',
      url: communityTopicApi.listUrl(params)
    })
  },
  /**
    TopicCate topicCate;
    Long refId;// 主题关联
    Set<String> attashments;// 配图
    String content; // 文字内容
    Customer lz;
   */
  add(params) {
    return axios({
      method: 'post',
      url: communityTopicApi.addUrl(),
      data: params
    })
  },
  delete(id) {
    return axios({
      method: 'delete',
      url: communityTopicApi.deleteUrl(id)
    })
  },
  get(id) {
    return axios({
      method: 'get',
      url: communityTopicApi.getUrl(id)
    })
  },
  liked(id) {
    return axios({
      method: 'put',
      url: communityTopicApi.likedUrl(id)
    })
  },
  likedCancel(id) {
    return axios({
      method: 'put',
      url: communityTopicApi.likedCancelUrl(id)
    })
  },
}
