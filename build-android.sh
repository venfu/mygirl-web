#!/usr/bin/env bash
## check env & package
command -v quasar >/dev/null 2>&1 || { echo >&2 "quasar uninstall? try: $ cnpm install -g cordova"; exit; }
sed -i "" "s/global.IS_ANDROID = false/global.IS_ANDROID = true/g"  ./src/gloabal.js
quasar build

## transfer resource
cd cordova/
mkdir -p www
cd www/
rm -rf *
cp -r ../../dist/* ./
cd ..
## build apk
cordova build android
cp ./platforms/android/build/outputs/apk/debug/android-debug.apk ../cordova/
mv android-debug.apk hg-latest.apk
## 生成hg-release.keystore
## keytool -genkey -v -keystore hg.keystore -alias hg.keystore -keyalg RSA -validity 20000
## keytool -importkeystore -srckeystore hg.keystore -destkeystore hg.keystore -deststoretype pkcs12
# cd ./cordova
# jarsigner -verbose -keystore hg.keystore -signedjar himygirl.apk android-debug.apk hg.keystore